# Maria DB docker-compose for a developer and SQL users

Docker-compose for MariaDB docker instance

To run mariadb you need to have on your PC/Mac/Server - docker & docker-compose installed.

```
docker-compose up -d
```


Get into mysql console:
```
docker exec -it maria mysql -uroot -prootpsw
```

Next steps:
1. Create Database `data_base_name`.
1. Create new user with `user_name` and grans access to `database_name`.
1. Get into a console using `user_name` and accessing `database_name`:
```
# Instead of {user_name} put user name without "{}"
# Instead of {user_password} put user password without "{}"
# Instead of {database_name} put database name without "{}"
docker exec -it maria mysql -u{user_name} -p{user_password} {database_name}
```

Basic script to create blank database `test` and user `test` with access to this:
```
./init_db.sh
```

To dump database use:
```
./dump.sh
```

To restore database use:
```
./restore.sh
```

### Note
The file `.env.example` as example, better create your local `.env` file.