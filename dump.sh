#!/bin/bash
if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
elif [ -f .env.example ]
then
  export $(cat .env.example | sed 's/#.*//g' | xargs)
fi

docker exec maria mysqldump -uroot -p${MYSQL_ROOT_PASSWORD} ${DB_NAME} > ./dump/dump_last.sql

now=$(date +"%FT_%H-%M-%S")
tar -czvf ./dump/dump_${now}.tar.gz ./dump/dump_last.sql