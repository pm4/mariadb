#!/bin/bash
if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
elif [ -f .env.example ]
then
  export $(cat .env.example | sed 's/#.*//g' | xargs)
fi

DB_NAME=${DB_NAME:-test1}
DB_USER_NAME=${DB_USER_NAME:-test1}
DB_USER_PASS=${DB_USER_PASS:-test1psw}

echo "CREATE DATABASE IF NOT EXISTS \`${DB_NAME}\`;
CREATE USER IF NOT EXISTS '${DB_USER_NAME}'@'%' IDENTIFIED BY '${DB_USER_PASS}';
USE \`${DB_NAME}\`;
GRANT ALL ON \`${DB_NAME}\`.* TO '${DB_USER_NAME}'@'%';" | docker exec -i maria mysql -uroot -p${MYSQL_ROOT_PASSWORD}