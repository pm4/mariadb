#!/bin/bash
./init_db.sh

if [ -f .env ]
then
  export $(cat .env | sed 's/#.*//g' | xargs)
elif [ -f .env.example ]
then
  export $(cat .env.example | sed 's/#.*//g' | xargs)
fi

docker exec -i maria mysql -uroot -p${MYSQL_ROOT_PASSWORD} ${DB_NAME} < ./dump/dump_last.sql